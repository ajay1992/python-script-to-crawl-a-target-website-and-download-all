"""written by ajay
ajay@meajay.in


Required Package: BeautifulSoup

imagecrawler.py
    Downlaod all images on the targeted site by crawling the site
    download to the folder image_crawler in the current working directory

Usage:
    
python imagecrawler.py 'http://www.codebin.in'

"""

from bs4 import BeautifulSoup
import urllib
import sys
from urllib import urlretrieve
import os
import urlparse


crawled={}
img={}

parsed = list(urlparse.urlparse(sys.argv[1]))

def download(url):
    filename=url.split("/")[-1]
    print "downloading image : "+filename
    outpath = os.path.join('image_crawler', filename)
    urlretrieve(url, outpath)
        
def crawl(s):
    global parsed
    global crawled
    global img
    ext=['.png','.jpg','.gif']
    print "Downloading images from: "+s
    html=urllib.urlopen(s)
    bs=BeautifulSoup(html.read(),'lxml')

    #dowload all images on current link
    for e in bs.findAll('img'):
        parsed[2]=e['src']
        url = e['src'] if e["src"].lower().startswith("http") else urlparse.urlunparse(parsed)
        if url not in img:
            download(url)
            img[url]=1

    #crawl current link    
    link=bs.findAll('a')
    for e in link:
        try:
            parsed[2]=e['href']
        except:
            continue
        url = e['href'] if e["href"].lower().startswith("http") else urlparse.urlunparse(parsed)
        if url not in crawled:
            try:
                #if url is of a image
                if url[-4:] in ext:
                    download(url)
                # crawl only if url is of targeted website
                elif url[:len(sys.argv[1])]==sys.argv[1]:
                    crawled[url]=1
                    crawl(url)
                else:
                    pass
            except:
                pass
                
crawled[sys.argv[1]]=1
crawl(sys.argv[1])
print "Crawling Complete!"
