"""written by ajay
ajay@meajay.in


Required Package: BeautifulSoup

imagecrawler.py
    Downlaod all images on the targeted site by crawling the site
    download to the folder image_crawler in the current working directory

Usage:
    
python imagecrawler.py 'http://www.codebin.in'

"""

from bs4 import BeautifulSoup
import urllib.request, urllib.parse, urllib.error
import sys
from urllib.request import urlretrieve
import os
import urllib.parse
from requests_html import HTMLSession

crawled={}
img={}
to_crawl = [sys.argv[1]]
parsed = list(urllib.parse.urlparse(sys.argv[1]))
session = HTMLSession()
#r = session.get(sys.argv[1])
def download(url):
    try:
        filename=url.split("/")[-1]
        print("downloading image : "+url)
        outpath = os.path.join('image_crawler', filename)
        urlretrieve(url, outpath)
    except:
        pass
        
def crawl():
    global parsed
    global crawled
    global img
    global to_crawl
    
    ext=['.png','.jpg','.gif']
    while len(to_crawl) > 0:
        curr_url = to_crawl.pop()
        if curr_url in crawled:
            continue
        else:
            crawled[curr_url]=True
            print("crawling :"+curr_url)
            r=session.get(curr_url)
            r.html.render(timeout=30000)
            bs=BeautifulSoup(r.html.raw_html,'lxml')

            #dowload all images on current link
            for image_tag in bs.findAll('img'):
                parsed[2]=image_tag['src']
                url = image_tag['src'] if image_tag["src"].lower().startswith("http") else urllib.parse.urlunparse(parsed)
                if url not in img and url[-4:] in ext:
                    download(url)
                    img[url]=1

            #crawl current link    
            link=bs.findAll('a')
            for e in link:
                try:
                    parsed[2]=e['href']
                except:
                    continue
                url = e['href'] if e["href"].lower().startswith("http") else urllib.parse.urlunparse(parsed)
                if url not in crawled:
                    try:
                        #if url is of a image
                        if url[-4:] in ext:
                            download(url)
                        # crawl only if url is of targeted website
                        elif url[:len(sys.argv[1])]==sys.argv[1]:
                            to_crawl.append(url)
                        else:
                            pass
                    except:
                        pass
                
crawl()
print("Crawling Complete!")

